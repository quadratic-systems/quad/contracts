# Quadratic (QUAD)

Quadratic (ticker symbol QUAD) is the utility token of the Quadratic ecosystem.

## Development

Clone this repository into your development environment:

```bash
git clone https://gitlab.com/quadratic-systems/tokens/quadratic.git
```

Install the 3rd party modules used in this project:

```bash
git submodule update --init --recursive
```

Set up your environment variables:

```bash
cp env.default .env
```

Update the .env variables with your custom settings.

### Testing

All smart contract functionality should be fully tested.

To launch the unit tests, run:

```bash
make test
```

You can also check the coverage of tests by running:

```bash
make coverage
```

## Deployment

The Quadratic contract can be deployed to various chains using the "deploy-*" targets.

### Locally

You will need to have a local development chain running in order to deploy locally. Anvil is the default choice but you can also run local development chain software such as Ganache also. Adjust the ANVIL_RPC_URL env-var (in the .env) if your chain software does not launch on the same default address as Anvil.

Assuming you are using Anvil as your local development chain, run the following:

```bash
anvil
make deploy-dev
```

### Testnet

You can also deploy the smart contract to an EVM-compatible test chain.

You will need to provide a chain RPC url to be able to deploy to a test chain. Update the .env's SEPOLIA_RPC_URL and ETHERSCAN_API_KEY variables with your own configuration.

The current "deploy-test" target requires a valid private key is stored in the Foundry key store. To load a new key into the key store, run the following:

```bash
cast wallet import Quadratic --interactive
```

Follow the prompts to complete the importation of your private key.

Next, deploy to test net:

```bash
make deploy-test
```

If successful, the contract will be deployed to the Sepolia test network and will be verified via Etherscan.

## Updating submodules

Sometimes, you will want to update the supporting modules which Quadratic imports.

To update a submodule, run `forge update <module-name>` where <module-name> is the name of the submodule being updated.

For example, if we want to update the OpenZeppelin library, we would run:

```bash
forge update openzeppelin-contracts
```

Once updated, check to make sure the updates don't break the current smart contract implementation:

```bash
forge build
```

You should see no errors if everything is running correctly.