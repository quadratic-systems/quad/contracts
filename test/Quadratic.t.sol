// SPDX-License-Identifier: GPL-3.0-or-later
pragma solidity ^0.8.25;

import {Test, console} from "forge-std/Test.sol";
import {Quadratic} from "../src/Quadratic.sol";
import {IERC20} from "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import {ERC20Permit} from "@openzeppelin/contracts/token/ERC20/extensions/ERC20Permit.sol";
import {MessageHashUtils} from "@openzeppelin/contracts/utils/cryptography/MessageHashUtils.sol";
import {IERC20Errors} from "@openzeppelin/contracts/interfaces/draft-IERC6093.sol";
import {IAccessControl} from "@openzeppelin/contracts/access/IAccessControl.sol";
import {SigUtils} from "./utils/SigUtils.sol";
import {QuadraticScript} from "../script/Quadratic.s.sol";

contract QuadraticTest is Test {
    SigUtils internal sigUtils;

    address deployer = address(this);

    uint256 bobPrivateKey = 0xB0B;
    uint256 alicePrivateKey = 0xA11CE;

    address bob = vm.addr(bobPrivateKey);
    address alice = vm.addr(alicePrivateKey);

    uint256 public constant ONE_MILLION = 1e6 * 1e18;

    Quadratic public token;

    function setUp() public {
        QuadraticScript script = new QuadraticScript();
        token = script.run();
        sigUtils = new SigUtils(token.DOMAIN_SEPARATOR());
    }

    function testRolesOnDeployment() public view {
        assertTrue(token.hasRole(token.DEFAULT_ADMIN_ROLE(), deployer));
        assertTrue(token.hasRole(token.MINTER_ROLE(), deployer));
    }

    function testZeroBalanceOnDeployment() public view {
        assertEq(token.totalSupply(), 0);
    }

    function testMinting() public {
        token.mint(deployer, ONE_MILLION);
        assertEq(token.totalSupply(), ONE_MILLION);
        assertEq(token.balanceOf(deployer), ONE_MILLION);
    }

    function testMintingEvent() public {
        vm.expectEmit();
        emit IERC20.Transfer(address(0), deployer, ONE_MILLION);
        token.mint(deployer, ONE_MILLION);
    }

    function testShouldNotMintIfNotMinterRole() public {
        vm.expectRevert(
            abi.encodeWithSelector(IAccessControl.AccessControlUnauthorizedAccount.selector, bob, token.MINTER_ROLE())
        );
        vm.prank(bob);
        token.mint(deployer, ONE_MILLION);
    }

    function testTransfer() public {
        token.mint(deployer, ONE_MILLION);
        token.transfer(bob, ONE_MILLION);
        assertEq(token.balanceOf(bob), ONE_MILLION);
    }

    function testTransferEvent() public {
        token.mint(deployer, ONE_MILLION);
        vm.expectEmit();
        emit IERC20.Transfer(deployer, bob, ONE_MILLION);
        token.transfer(bob, ONE_MILLION);
    }

    function testAllowance() public {
        token.mint(deployer, ONE_MILLION);
        token.approve(bob, ONE_MILLION);
        assertEq(token.allowance(deployer, bob), ONE_MILLION);
    }

    function testAllowanceEvent() public {
        token.mint(deployer, ONE_MILLION);

        vm.expectEmit();
        emit IERC20.Approval(deployer, bob, ONE_MILLION);
        token.approve(bob, ONE_MILLION);
    }

    function testTransferFrom() public {
        token.mint(deployer, ONE_MILLION);
        token.approve(bob, ONE_MILLION);
        vm.prank(bob);
        token.transferFrom(deployer, alice, ONE_MILLION);
        assertEq(token.balanceOf(alice), ONE_MILLION);
    }

    function testTransferFromWithoutAllowance() public {
        token.mint(deployer, ONE_MILLION);
        vm.expectRevert(abi.encodeWithSelector(IERC20Errors.ERC20InsufficientAllowance.selector, bob, 0, ONE_MILLION));
        vm.prank(bob);
        token.transferFrom(deployer, alice, ONE_MILLION);
    }

    function testPermit() public {
        SigUtils.Permit memory permit =
            SigUtils.Permit({owner: bob, spender: alice, value: ONE_MILLION, nonce: 0, deadline: 1 days});

        bytes32 digest = sigUtils.getTypedDataHash(permit);

        (uint8 v, bytes32 r, bytes32 s) = vm.sign(bobPrivateKey, digest);

        token.permit(permit.owner, permit.spender, permit.value, permit.deadline, v, r, s);

        assertEq(token.allowance(bob, alice), ONE_MILLION);
        assertEq(token.nonces(bob), 1);
    }

    function testExpiredPermit() public {
        SigUtils.Permit memory permit = SigUtils.Permit({
            owner: bob,
            spender: alice,
            value: ONE_MILLION,
            nonce: token.nonces(bob),
            deadline: 1 days
        });

        bytes32 digest = sigUtils.getTypedDataHash(permit);

        (uint8 v, bytes32 r, bytes32 s) = vm.sign(bobPrivateKey, digest);

        vm.warp(1 days + 1 seconds); // fast forward one second past the deadline
        vm.expectRevert(abi.encodeWithSelector(ERC20Permit.ERC2612ExpiredSignature.selector, 1 days));
        token.permit(permit.owner, permit.spender, permit.value, permit.deadline, v, r, s);
    }

    function testInvalidSigner() public {
        SigUtils.Permit memory permit = SigUtils.Permit({
            owner: bob,
            spender: alice,
            value: ONE_MILLION,
            nonce: token.nonces(bob),
            deadline: 1 days
        });

        bytes32 digest = sigUtils.getTypedDataHash(permit);

        (uint8 v, bytes32 r, bytes32 s) = vm.sign(alicePrivateKey, digest); // spender signs owner's approval

        vm.expectRevert(abi.encodeWithSelector(ERC20Permit.ERC2612InvalidSigner.selector, alice, bob));
        token.permit(permit.owner, permit.spender, permit.value, permit.deadline, v, r, s);
    }

    function testInvalidNonce() public {
        SigUtils.Permit memory permit = SigUtils.Permit({
            owner: bob,
            spender: alice,
            value: ONE_MILLION,
            nonce: 1, // owner nonce stored on-chain is 0
            deadline: 1 days
        });

        bytes32 digest = sigUtils.getTypedDataHash(permit);

        (uint8 v, bytes32 r, bytes32 s) = vm.sign(bobPrivateKey, digest);

        bytes32 structHash = keccak256(
            abi.encode(
                keccak256("Permit(address owner,address spender,uint256 value,uint256 nonce,uint256 deadline)"),
                bob,
                alice,
                ONE_MILLION,
                token.nonces(bob),
                1 days
            )
        );

        bytes32 hash = MessageHashUtils.toTypedDataHash(token.DOMAIN_SEPARATOR(), structHash);

        address invalidSigner = ecrecover(hash, v, r, s);

        vm.expectRevert(abi.encodeWithSelector(ERC20Permit.ERC2612InvalidSigner.selector, invalidSigner, bob));
        token.permit(permit.owner, permit.spender, permit.value, permit.deadline, v, r, s);
    }

    function testSignatureReplay() public {
        SigUtils.Permit memory permit =
            SigUtils.Permit({owner: bob, spender: alice, value: ONE_MILLION, nonce: 0, deadline: 1 days});

        bytes32 digest = sigUtils.getTypedDataHash(permit);

        (uint8 v, bytes32 r, bytes32 s) = vm.sign(bobPrivateKey, digest);

        bytes32 structHash = keccak256(
            abi.encode(
                keccak256("Permit(address owner,address spender,uint256 value,uint256 nonce,uint256 deadline)"),
                bob,
                alice,
                ONE_MILLION,
                token.nonces(bob) + 1,
                1 days
            )
        );

        bytes32 hash = MessageHashUtils.toTypedDataHash(token.DOMAIN_SEPARATOR(), structHash);

        address invalidSigner = ecrecover(hash, v, r, s);

        token.permit(permit.owner, permit.spender, permit.value, permit.deadline, v, r, s);

        vm.expectRevert(abi.encodeWithSelector(ERC20Permit.ERC2612InvalidSigner.selector, invalidSigner, bob));
        token.permit(permit.owner, permit.spender, permit.value, permit.deadline, v, r, s);
    }
}
