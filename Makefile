.PHONY: help lint test clean

-include .env

help:
	@echo "Usage: make <target>"
	@echo ""
	@echo "lint"
	@echo "    Format code."
	@echo ""
	@echo "test"
	@echo "    Run unit tests."
	@echo ""
	@echo "coverage"
	@echo "    Generates a coverage report."
	@echo ""	
	@echo "clean"
	@echo "    Run forge clean."
	@echo ""
	@echo "export-abi"
	@echo "    Generates an ES-compatible abi file. For import into dapps."
	@echo ""
	@echo "deploy-dev"
	@echo "    Deploy smart contracts to a dev chain such as Anvil. Configured "
	@echo "    using ANVIL_RPC_URL."
	@echo ""
	@echo "deploy-test"
	@echo "    Deploy smart contracts to a testnet. Configured using "
	@echo "    SEPOLIA_RPC_URL. (smart contracts will also be verified on "
	@echo "    etherscan using ETHERSCAN_API_KEY)"
	@echo ""

lint:
	forge fmt

test:
	forge test -vv
	
coverage:
	forge coverage
	
clean:
	forge clean

export-abi:
	rm -Rf ./abi
	mkdir ./abi
	# The following is piping additional output into the file, breaking the js.
	# ( echo -n "export default "; forge build --silent && jq '.abi' ./out/QuId.sol/Quadratic.json; ) > ./abi/quadratic.js
	echo -n "export default " > ./abi/quadratic.js
	forge build --silent && jq '.abi' ./out/Quadratic.sol/Quadratic.json >> ./abi/quadratic.js

deploy-dev:
	# private-key from Anvil deterministic private keys.
	forge script ./script/Quadratic.s.sol --broadcast --private-key 0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80 --rpc-url=$(ANVIL_RPC_URL)

deploy-test:
	# private key from cast wallet
	forge script ./script/Quadratic.s.sol --broadcast --rpc-url $(SEPOLIA_RPC_URL) --account Quadratic --sender $(SEPOLIA_SENDER) --verify -vvv
