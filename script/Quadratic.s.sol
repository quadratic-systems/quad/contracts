// SPDX-License-Identifier: GPL-3.0-or-later
pragma solidity ^0.8.25;

import {Script, console} from "forge-std/Script.sol";
import {Quadratic} from "../src/Quadratic.sol";

contract QuadraticScript is Script {
    function run() external returns (Quadratic) {
        vm.startBroadcast();
        Quadratic token = new Quadratic(msg.sender);
        vm.stopBroadcast();

        return token;
    }
}
